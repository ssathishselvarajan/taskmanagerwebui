import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { ViewtaskHandler } from './TaskHandler/ViewTaskhandler/viewtaskHandler.component';
import { AddtaskhandlerComponent } from './TaskHandler/addtaskhandler/addtaskhandler.component';
import { ViewTaskFilter } from './TaskHandler/viewtaskfilter/viewtaskfilter.component';
import { UpdateTaskhandlerComponent } from './TaskHandler/update-taskhandler/update-taskhandler.component';
import{MenuComponent } from './TaskHandler/menu/menu.component'


const appRoutes: Routes = [
    {path : 'AddTask', component : AddtaskhandlerComponent},
    {path : 'UpdateTask/:TaskID', component : UpdateTaskhandlerComponent},
    {path : 'ViewTask', component : ViewtaskHandler},
    {path : '', component : AddtaskhandlerComponent},
    {path : '', component : MenuComponent},
    ];

    @NgModule({
      imports: [RouterModule.forRoot(appRoutes)],
      exports: [RouterModule]
    })
    export class AppRoutingModule { }