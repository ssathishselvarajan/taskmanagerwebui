import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import{ TaskTable} from 'src/app/model/TaskTable';
import {Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Console } from '@angular/core/src/console';
import { HttpParams } from '@angular/common/http/src/params';
const httpOptions = {headers : new HttpHeaders({'Content-Type' : 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class TaskhandlerApiserviceService {

  data : String;
  constructor(private http: HttpClient) { }

  getTasks():Observable<TaskTable[]>{
    return this.http.get<TaskTable[]>('http://localhost/TaskHandlerAPIservice/api/values/' + 'GetTaskDetails');
  }

  getTaskByID(taskID: string):Observable<any>{
    return this.http.get<any>('http://localhost/TaskHandlerAPIservice/api/values/' + 'GetTaskDetails?id='+ taskID);

  }

  addTask(taskToAdd : TaskTable) : Observable<TaskTable>{
     return this.http.post<TaskTable>('http://localhost/TaskHandlerAPIservice/api/values/' + 'AddTaskDetails',  taskToAdd, httpOptions );
  }

  updateTask(taskToUpdate : TaskTable):Observable<TaskTable>{
    return this.http.put<TaskTable>('http://localhost/TaskHandlerAPIservice/api/values/' + 'UpdateTaskDetails',  taskToUpdate, httpOptions);
  }

  deleteTask(taskID: number):Observable<any>{
    debugger;
      return this.http.delete<any>('http://localhost/TaskHandlerAPIservice/api/values/' + 'DeleteTask?id='+ taskID);
  }
}
