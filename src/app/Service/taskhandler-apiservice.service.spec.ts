import { TestBed } from '@angular/core/testing';
import {HttpClientModule} from '@angular/common/http';
import { TaskhandlerApiserviceService } from './taskhandler-apiservice.service';

describe('TaskhandlerApiserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports:[HttpClientModule],
  }));

  it('should be created', () => {
    const service: TaskhandlerApiserviceService = TestBed.get(TaskhandlerApiserviceService);
    expect(service).toBeTruthy();
  });
});
