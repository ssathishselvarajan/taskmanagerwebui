export class TaskTable {
    Task: string;
    TaskID: number;
    Priority :number;
    ParentTask:string;
    ParentTaskID: number;
    StartDate:string;
    EndDate:string;
}