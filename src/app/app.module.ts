import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewtaskHandler } from './TaskHandler/ViewTaskhandler/viewtaskHandler.component';
import { AddtaskhandlerComponent } from './TaskHandler/addtaskhandler/addtaskhandler.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { MenuComponent } from './TaskHandler/menu/menu.component';
import{ HttpClientModule } from '@angular/common/http';
import { ViewTaskFilter } from './TaskHandler/viewtaskfilter/viewtaskfilter.component';
import { UpdateTaskhandlerComponent } from './TaskHandler/update-taskhandler/update-taskhandler.component';
import { TaskhandlerApiserviceService } from 'src/app/Service/taskhandler-apiservice.service';


@NgModule({
  declarations: [
    AppComponent,
    ViewtaskHandler,
    AddtaskhandlerComponent,
    MenuComponent,
    ViewTaskFilter,
    UpdateTaskhandlerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,ReactiveFormsModule,HttpClientModule,RouterModule,AppRoutingModule
  ],
  exports: [
    BrowserModule,
    FormsModule,ReactiveFormsModule
  ],
  providers: [TaskhandlerApiserviceService],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
