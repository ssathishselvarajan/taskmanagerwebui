import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewtaskfilterComponent } from './viewtaskfilter.component';

describe('ViewtaskfilterComponent', () => {
  let component: ViewtaskfilterComponent;
  let fixture: ComponentFixture<ViewtaskfilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewtaskfilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewtaskfilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
