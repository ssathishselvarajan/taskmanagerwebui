import { Pipe, PipeTransform } from '@angular/core';
import { TaskTable } from 'src/app/Model/TaskTable';
import { forEach } from '@angular/router/src/utils/collection';

@Pipe({
  name: 'viewTaskFilter', pure:false
})
export class ViewTaskFilter implements PipeTransform {

  transform(taskList: Array<TaskTable>, taskName: any, taskParent:any, taskPriorityFrom:any, taskPriorityTo:any, taskStartDate:any, taskEndDate:any): any {
    // let filteredTaskList : TaskTable[];
    // for(let i =0; i< taskList.length; i++){
    //   // if(taskList[i].task.toLocaleLowerCase().includes(taskName.toLocaleLowerCase()) && taskList[i].parent_ID.toLocaleLowerCase().includes(taskParent.toLocaleLowerCase()) &&
    //   // taskList[i].start_Date.toLocaleDateString().includes(taskStartDate.toLocaleDateString()) && taskList[i].end_Date.toLocaleDateString().includes(taskEndDate.toLocaleDateString()))
    //   {
    //       filteredTaskList.push(taskList[i]);
    //   }
    // }

    if (taskList && taskList.length){
      return taskList.filter(item =>{
          if (taskName && item.Task.toLowerCase().indexOf(taskName.toLowerCase()) === -1){
              return false;
          }
          if (taskParent && item.ParentTask.toLowerCase().indexOf(taskParent.toLowerCase()) === -1){
              return false;
          }
          if (taskStartDate && item.StartDate.toLocaleString().indexOf(taskStartDate.toLocaleString()) === -1){
              return false;
          }
          if (taskEndDate && item.EndDate.toLocaleString().indexOf(taskEndDate.toLocaleString()) === -1){
            return false;
        }
        if (taskPriorityTo && item.Priority.toLocaleString().indexOf(taskPriorityTo.toLocaleString()) === -1){
            return false;
        }
        return true;
     })
  }
  else{
      return taskList;
  }
  }

}
