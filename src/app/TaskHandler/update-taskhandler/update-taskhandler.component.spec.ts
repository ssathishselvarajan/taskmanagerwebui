import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { UpdateTaskhandlerComponent } from './update-taskhandler.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import {HttpClientModule} from '@angular/common/http';

describe('UpdateTaskhandlerComponent', () => {
  let component: UpdateTaskhandlerComponent;
  let fixture: ComponentFixture<UpdateTaskhandlerComponent>;

  // let getAllTasksSpy : any;
  // let testAllTasks: any;

  beforeEach(async(() => {
    //const taskServiceSpy = jasmine.createSpyObj('ServiceTaskService',['updateTask'] );
   // getAllTasksSpy = taskServiceSpy.updateTask.and.returnValue(of(testAllTasks));
    TestBed.configureTestingModule({
      imports:[FormsModule, ReactiveFormsModule,RouterTestingModule,HttpClientModule],
      declarations: [ UpdateTaskhandlerComponent ],
      providers:[UpdateTaskhandlerComponent]
    })
    .compileComponents();
  }));

 
  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTaskhandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
