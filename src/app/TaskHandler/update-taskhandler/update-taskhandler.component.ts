import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import{ TaskhandlerApiserviceService } from 'src/app/Service/taskhandler-apiservice.service'
import {ActivatedRoute} from '@angular/router';
import {DatePipe} from '@angular/common';
import{ TaskTable } from 'src/app/Model/Tasktable'


@Component({
  selector: 'app-update-taskhandler',
  templateUrl: './update-taskhandler.component.html',
  styleUrls: ['./update-taskhandler.component.css']
})
export class UpdateTaskhandlerComponent implements OnInit {
  task: string;
  taskid:number;
  parenttaskid:number;
  priority :number;
  parentTask:string;
  startDate:any;
  endDate:any;
  public Updatetask: TaskTable[]=[];
  showMsg:boolean;
  constructor(private taskService :TaskhandlerApiserviceService , private route : ActivatedRoute) {
    //this.Updatetask = new TaskTable();
   }

  ngOnInit() {
    this.loadTask()
  }
 loadTask(){
  
   if(this.route.snapshot.paramMap.get('TaskID')){
    //this.taskService.getTaskByID(this.route.snapshot.paramMap.get('TaskID')).subscribe(data=> this.Updatetask=data);
    this.taskService.getTaskByID(this.route.snapshot.paramMap.get('TaskID')).subscribe(data=>{debugger;this.Updatetask =data
      this.task =this.Updatetask[0].Task;
      this.priority =this.Updatetask[0].Priority;
      this.startDate =this.Updatetask[0].StartDate;
      this.endDate =this.Updatetask[0].EndDate;
      this.taskid = this.Updatetask[0].TaskID;
      this.parentTask =this.Updatetask[0].ParentTask;
      this.parenttaskid =this.Updatetask[0].ParentTaskID;
    });
    //this.TaskService.getTasks().subscribe(data=>{this.allTask =data});
   }
   else{
     console.log("Task ID not found");
   }
    // this.task =this.Updatetask.Task;
    // this.priority =this.Updatetask.Priority;
    // this.startDate =this.Updatetask.StartDate;
    // this.endDate =this.Updatetask.EndDate;
    // this.taskid = this.Updatetask.TaskID;
 }

 onSubmit(){
   debugger;
  this.Updatetask[0].Task= this.task;
  this.Updatetask[0].Priority=this.priority;
  this.Updatetask[0].StartDate= this.startDate;
  this.Updatetask[0].EndDate=this.endDate ;
  this.Updatetask[0].TaskID=this.taskid ;
  this.Updatetask[0].ParentTask=this.parentTask;
  this.Updatetask[0].ParentTaskID= this.parenttaskid;
  this.taskService.updateTask(this.Updatetask[0]).subscribe(data=> {console.log("Task updated.")});
  this.showMsg =true;
  this.resetTaskForm();
 }

 resetTaskForm()
 {
  this.task="";
  this.priority=0;
  this.startDate="";
  this.endDate="" ;
  this.taskid=0;
  this.parentTask="";
  this.parenttaskid=0;

 }

}
