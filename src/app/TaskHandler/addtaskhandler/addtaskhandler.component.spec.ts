import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtaskhandlerComponent } from './addtaskhandler.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import{ TaskhandlerApiserviceService } from 'src/app/Service/taskhandler-apiservice.service'
import { of } from 'rxjs';

describe('AddtaskhandlerComponent', () => {
  let component: AddtaskhandlerComponent;
  let fixture: ComponentFixture<AddtaskhandlerComponent>;

  let getAllTasksSpy : any;
  let testAllTasks: any;

  beforeEach(async(() => {
    const taskServiceSpy = jasmine.createSpyObj('TaskhandlerApiserviceService',['addTask'] );
    getAllTasksSpy = taskServiceSpy.addTask.and.returnValue(of(testAllTasks));
    TestBed.configureTestingModule({
      imports:[FormsModule, ReactiveFormsModule,RouterTestingModule, HttpClientModule],
      declarations: [ AddtaskhandlerComponent ],
      providers:[AddtaskhandlerComponent, {provide:TaskhandlerApiserviceService, useValue:taskServiceSpy}]
    })
    .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(AddtaskhandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
