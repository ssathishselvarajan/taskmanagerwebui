import { Component, OnInit } from '@angular/core';
import {Validators} from '@angular/forms';
import{ TaskTable} from 'src/app/Model/TaskTable'
import{ TaskhandlerApiserviceService } from 'src/app/Service/taskhandler-apiservice.service'


@Component({
  selector: 'app-addtaskhandler',
  templateUrl: './addtaskhandler.component.html',
  styleUrls: ['./addtaskhandler.component.css']
})
export class AddtaskhandlerComponent implements OnInit {
  task: string;
  taskid:number;
  parenttaskid:number;
  priority :number;
  parentTask:string;
  startDate:any;
  endDate:any;
  Status:string;
  showMsg:boolean;

  result : string;
  public AddNewtask: TaskTable;
 constructor(private TaskService :TaskhandlerApiserviceService )
  { 
    this.AddNewtask = new TaskTable();
      
}
  ngOnInit() {
    this.resetTaskForm();
  }
  onSubmit(){
    debugger; 
    this.AddNewtask.Task = this.task;
    this.AddNewtask.Priority = this.priority;
    this.AddNewtask.ParentTask = this.parentTask;
    this.AddNewtask.StartDate = this.startDate;
    this.AddNewtask.EndDate = this.endDate;
      //this.newTask = this.taskForm.value;
      this.TaskService.addTask(this.AddNewtask).subscribe(data=> { this.showMsg= true;});
      this.resetTaskForm();
  }

  resetTaskForm(){
    this.task="";
    this.priority=0;
    this.startDate="";
    this.endDate="" ;
    this.taskid=0;
    this.parentTask="";
    this.parenttaskid=0;
  }
}
