import { Component, OnInit } from '@angular/core';
import{ TaskTable } from 'src/app/Model/Tasktable'
import { Data } from '@angular/router/src/config';
import {Observable} from 'rxjs';
import{ TaskhandlerApiserviceService } from 'src/app/Service/taskhandler-apiservice.service'

@Component({
  selector: 'app-home',
  templateUrl: './viewtaskHandler.component.html',
  styleUrls: ['./viewtaskHandler.component.css']
})
export class ViewtaskHandler implements OnInit {
  allTask : any[];
  txtTask : string;
  txtParentTask : number;
  txtPrioriyFrom : number;
  txtPrioriyTo : number;
  dtStartDate : any;
  dtEndDate : any;
  showMsg:boolean;
  constructor( private TaskService :TaskhandlerApiserviceService ) { }  //

  getAllTasks()
  {  
    //this.allTask.getTasks().subscribe(data=>{this.usersResult = data});
    debugger;
    this.TaskService.getTasks().subscribe(data=>{this.allTask =data});
  }

  endTask(TaskID:number)
  {
      debugger;
       this.TaskService.deleteTask(TaskID).subscribe((data)=>{this.TaskService.getTasks().subscribe(data=>{this.allTask =data});this.showMsg=true;});
  }

  ngOnInit() {
    debugger;
  this.getAllTasks();
  }

}
