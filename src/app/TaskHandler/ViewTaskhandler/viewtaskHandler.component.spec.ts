import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import{ TaskTable } from 'src/app/Model/Tasktable'
import { ViewtaskHandler } from './viewtaskHandler.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import{ TaskhandlerApiserviceService } from 'src/app/Service/taskhandler-apiservice.service'
import{RouterTestingModule} from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';
import { ViewTaskFilter } from 'src/app/TaskHandler/viewtaskfilter/viewtaskfilter.component';

describe('viewtaskHandler', () => {
  let component: ViewtaskHandler;
  let fixture: ComponentFixture<ViewtaskHandler>;

  let getAllTasksSpy : any;
  let testAllTasks: TaskTable[];

  beforeEach(async(() => {
    const taskServiceSpy = jasmine.createSpyObj('TaskhandlerApiserviceService',['getTasks'] );
    getAllTasksSpy = taskServiceSpy.getTasks.and.returnValue(of(testAllTasks));
    TestBed.configureTestingModule({
      imports:[FormsModule,ReactiveFormsModule,RouterTestingModule],
      declarations: [ ViewtaskHandler,ViewTaskFilter],
      providers:[ViewtaskHandler, {provide:TaskhandlerApiserviceService, useValue:taskServiceSpy}]
    })
    .compileComponents();
    component = TestBed.get(ViewtaskHandler);
  }));

 

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewtaskHandler);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Test_GetAllTasks', () =>{
    component.ngOnInit();
 expect(getAllTasksSpy.calls.any()).toBe(true, 'GetAllTasks called');
  })

});
